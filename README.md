## Connection informations

```
sqlite3 myBDE.sql
```

## API informations

### users

<!-- ok -->
- GET: **/users** 
<!-- ok -->
- GET: **/user/:id**
<!-- ok -->
- POST: **/user/:id**

### projects

<!-- ok -->
- GET **/projects?status?limit?category**
  {
  status: **text**,
  limit: **integer**,
  category: **text**
  }
<!-- ok -->
- GET **/project/:id**
<!-- ok -->
- POST **/project**
<!-- ok -->
- POST **/project** {
  vote: **text**,
  id: **number**
}

### categories

<!-- ok -->
- GET: **/categories**
<!-- ok -->
- GET: **/category/:id**
<!-- ok -->
- GET: **/category/:name**
<!-- ok -->
- POST **/category/**
