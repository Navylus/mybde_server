from sqlite3 import Error
import sqlite3
import os
from flask_jwt_simple import (
    JWTManager, jwt_required, create_jwt, get_jwt_identity
)
from flask import Flask, jsonify, request
import config as config
from controllers.users import User_controller
from controllers.categories import Category_controller
from controllers.projects import Project_controller
from flask_cors import CORS, cross_origin


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = config.secret
app.config['CORS_HEADERS'] = 'Content-Type'
jwt = JWTManager(app)
conn = create_connection(config.db_file)
cors = CORS(app)

# init controllers
user_controller = User_controller(conn)
category_controller = Category_controller(conn)
project_controller = Project_controller(conn)


@app.route('/')
def hello():
    return "Hello on myBDE API!"

# login routes
@app.route('/auth', methods=["GET", "POST"])
@cross_origin()
def login():
    if request.method == 'POST':
        data = request.get_json()
        print(data)
        user_response = user_controller.authenticate(data)
        return jsonify(user_response)
    else:
        return jsonify({"message": "Not allowed", "code": 500}), 500


@app.route('/protected', methods=['GET'])
@cross_origin()
@jwt_required
def protected():
    # Access the identity of the current user with get_jwt_identity
    return jsonify({'hello_from': get_jwt_identity()}), 200


# users routes
@app.route('/users', methods=["GET", "POST"])
@cross_origin()
@jwt_required
def get_users():
    if request.method == 'POST':
        data = request.get_json()
        user_response = user_controller.insert_user(data)
        return jsonify(user_response)
    else:
        users = user_controller.get_users()
        return jsonify({"data": users, "code": 200}), 200


@app.route('/user/<id>')
@cross_origin()
@jwt_required
def get_user_by_id(id):
    user = user_controller.get_user_id(id)
    if len(user) <= 0:
        return jsonify({"message": "This user was not found. check the id", "code": 204}), 204
    return jsonify({"data": user, "code": 200}), 200

# categories routes


@app.route('/category', methods=["GET", "POST"])
@cross_origin()
@jwt_required
def get_category():
    if request.method == 'POST':
        data = request.get_json()
        category_response = category_controller.insert_category(data)
        return jsonify(category_response)
    else:
        categories = category_controller.get_categories()
        return jsonify({"data": categories, "code": 200}), 200


@app.route('/category/<id>')
@cross_origin()
@jwt_required
def get_category_by_id(id):
    category = category_controller.get_category_id(id)
    if len(category) <= 0:
        return jsonify({"message": "This category was not found. check the id", "code": 204}), 204
    return jsonify({"data": category, "code": 200}), 200

# projects routes


@app.route('/project', methods=["GET", "POST"])
@cross_origin()
@jwt_required
def get_project():
    if request.method == 'POST':
        data = request.get_json()
        project_response = project_controller.insert_project(data)
        return jsonify(project_response)
    else:
        status = request.args.get('status')
        limit = request.args.get('limit')
        category = request.args.get('category')
        projects = project_controller.get_projects(status, limit, category)
        return jsonify({"data": projects, "code": 200}), 200


@app.route('/vote', methods=["POST"])
@cross_origin()
@jwt_required
def vote_project():
    if request.method == 'POST':
        data = request.get_json()
        project_response = project_controller.vote_project(data)
        return jsonify(project_response), 200
    else:
        return jsonify({"message": "GET not allowed", "code": 500}), 500


@app.route('/project/<id>')
@cross_origin()
@jwt_required
def get_project_by_id(id):
    project = project_controller.get_project_id(id)
    if len(project) <= 0:
        return jsonify({"message": "This project was not found. check the id", "code": 204}), 204
    return jsonify({"data": project, "code": 200}), 200


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
