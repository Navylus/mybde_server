CREATE TABLE User
(
    id INTEGER PRIMARY KEY,
    username TEXT,
    firstname TEXT,
    lastname TEXT,
    creation_date TEXT,
    birthdate TEXT,
    formation TEXT,
    password TEXT,
    email TEXT UNIQUE
);

INSERT INTO User (username ,
                  firstname ,
                  lastname ,
                  creation_date ,
                  birthdate ,
                  email ,
                  password ,
                  formation )
VALUES( "navylus" ,
        "sulivan" ,
        "gorjao" ,
        "02/02/2020" ,
        "12/09/1997" ,
        "sg@gmail.com",
        "12ez3",
        "MDSM2" );

INSERT INTO User (username ,
                  firstname ,
                  lastname ,
                  creation_date ,
                  birthdate ,
                  email ,
                  password ,
                  formation )
VALUES( "jqq" ,
        "Jean" ,
        "Random" ,
        "02/02/2020" ,
        "29/12/1997" ,
        "jjq@gmail.com",
        "234aaz567",
        "MDSM1" );

CREATE TABLE Category
(
    id INTEGER PRIMARY KEY,
    title TEXT
);

INSERT INTO Category (title)
VALUES( "Soirée" );
INSERT INTO Category (title)
VALUES( "Jeux Vidéo" );
INSERT INTO Category (title)
VALUES( "Humanitaire" );
INSERT INTO Category (title)
VALUES( "Formation" );
INSERT INTO Category (title)
VALUES( "Afterwork" );
INSERT INTO Category (title)
VALUES( "Séjours" );
INSERT INTO Category (title)
VALUES( "Sortie" );
INSERT INTO Category (title)
VALUES( "Autres" );

CREATE TABLE Project
(
    id INTEGER PRIMARY KEY,
    title TEXT,
    start_date TEXT,
    nb_attendee_min INTEGER,
    nb_attendee_max INTEGER, 
    description TEXT,
    isPrivate INTEGER,
    status TEXT,
    nb_vote INTEGER,
    nb_up_vote INTEGER,
    category INTEGER
);

INSERT INTO Project (
    title,
    start_date,
    nb_attendee_min,
    nb_attendee_max, 
    description,
    isPrivate,
    status,
    nb_vote,
    nb_up_vote,
    category
)VALUES(
    "Test Project",
    "25/02/2020",
    "100",
    "200",
    "Un projet plutot cool je trouve",
    0,
    "A valider",
    100,
    80,
    1
);