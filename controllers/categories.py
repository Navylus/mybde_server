import sqlite3
import config as config


class Category_controller:
    def __init__(self, conn):
        self.conn = conn
        self.conn.row_factory = sqlite3.Row

    def get_categories(self):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM Category")

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def get_category_id(self, id):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM Category WHERE id = {}".format(id))

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def get_categories_title(self, title):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM Category WHERE title = '{}'".format(title))

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def insert_category(self, data):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            title = data["title"].encode("utf-8")
            if len(self.get_categories_title(title)) > 0:
                return {"message": "category already exist", "code": 304}
            cur.execute("""INSERT INTO Category(title)
                        VALUES("{}")
                        """.format(title))
            return {"message": "{} has successfuly been inserted".format(title), "code": 300}
