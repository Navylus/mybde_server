import sqlite3
import config as config

class Project_controller:
    def __init__(self, conn):
        self.conn = conn
        self.conn.row_factory = sqlite3.Row

    def get_projects(self, status, limit, category):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            if status:
                if category:
                    cur.execute(
                        "SELECT * FROM Project WHERE status = '{}' AND category = {}".format(status, category))
                else:
                    cur.execute(
                        "SELECT * FROM Project WHERE status = '{}'".format(status))
            else:
                if category:
                    cur.execute(
                        "SELECT * FROM Project WHERE category = {}".format(category))
                else:
                    cur.execute("SELECT * FROM Project")

            rows = cur.fetchall()

            if limit:
                for i in range(0, limit):
                    if i < len(rows):
                        res.append(dict(rows[i]))
            else:
                for row in rows:
                    res.append(dict(row))

            return res

    def get_project_id(self, id):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM project WHERE id = {}".format(id))

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def vote_project(self, data):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            id = data["id"].encode("utf-8")
            vote = data["vote"].encode("utf-8")
            cur = conn.cursor()
            cur.execute("SELECT * FROM project WHERE id = {}".format(id))
            obj = cur.fetchall()[0]
            up_vote = obj[9]
            total_vote = obj[8]
            print(obj)
            if vote == "plus":
                curr.execute("""
                            UPDATE project
                            SET nb_vote = {},
                                nb_up_vote = {}
                            WHERE
                                id = {}""".format(total_vote + 1,
                                                up_vote + 1, id))
            else:
                curr.execute("""
                UPDATE project
                SET nb_vote = {}
                WHERE
                    id = {}""".format(total_vote + 1, id))

            return {"message": "project {} has been updated".format(id), "code": 300}

    def insert_project(self, data):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            print(data)
            title = data["title"]
            start_date = data["start_date"]
            nb_attendee_min = data["nb_attendee_min"]
            nb_attendee_max = data["nb_attendee_max"]
            description = data["description"]
            isPrivate = data["isPrivate"]
            pr = 0
            if isPrivate == "True":
                pr = 1
            category = data["category"]
            cur.execute("""INSERT INTO Project (
                                title, start_date, nb_attendee_min, nb_attendee_max, 
                                description, isPrivate, status, nb_vote, nb_up_vote, category
                            )VALUES(
                                "{}","{}",{},{},"{}",{},"A valider",0,0,{}
                            );
                        """.format(title, start_date, nb_attendee_min, nb_attendee_max,
                                description, pr, category))
            conn.commit()
            return {"message": "project {} has successfuly been inserted".format(title), "code": 300}
