import sqlite3
import config as config
from datetime import date
from flask_jwt_simple import (create_jwt)


class User_controller:
    def __init__(self, conn):
        self.conn = conn
        self.conn.row_factory = sqlite3.Row

    def authenticate(self, data):
        email = data["email"]["text"]
        password = data["password"]["text"]
        print(email)
        with sqlite3.connect(config.db_file) as con:
            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute('SELECT * FROM User WHERE email = "{}"'.format(email))
            users = cur.fetchall()
            if len(users) > 0:
                user = dict(users[0])
                print(user["password"])
                print(password)
                if user["password"] == password:
                    ret = {'jwt': create_jwt(
                        identity="{}{}".format(email, password))}
                    return ret
            return ({"message": "Bad username or password", "code": 400})

    def get_users(self):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM User")

            rows = cur.fetchall()

            for row in rows:
                res.append(dict(row))

        return res

    def get_user_id(self, id):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM User WHERE id = {}".format(id))

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def get_user_email(self, email):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            res = []
            cur = conn.cursor()
            cur.execute("SELECT * FROM User WHERE email = ?", (email))

            rows = cur.fetchall()
            for row in rows:
                res.append(dict(row))

            return res

    def insert_user(self, data):
        with sqlite3.connect(config.db_file) as conn:
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            today = date.today()
            today = today.strftime("%d/%m/%y")
            username = data["username"].encode("utf-8")
            firstname = data["firstname"].encode("utf-8")
            lastname = data["lastname"].encode("utf-8")
            creation_date = today
            birthdate = data["birthdate"].encode("utf-8")
            email = data["email"].encode("utf-8")
            password = data["password"].encode("utf-8")
            formation = data["formation"].encode("utf-8")
            if len(self.get_user_email(email)) > 0:
                return {"message": "email already exist", "code": 304}
            cur.execute("""INSERT INTO User(username, firstname, lastname,
                                            creation_date, birthdate, email, password, formation)
                        VALUES("{}","{}","{}","{}","{}","{}","{}","{}")
                        """.format(username, firstname, lastname,
                                creation_date, birthdate, email, password, formation))
            return {"message": "{} has successfuly been inserted".format(username), "code": 300}
